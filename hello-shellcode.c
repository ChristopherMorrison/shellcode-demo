#include <unistd.h>
#include <sys/syscall.h>

void _start() {
    // shellcode example
    char string[] = "hello world\n";
    syscall(SYS_write, 1, string, sizeof(string));
    syscall(SYS_exit, 0);
}
