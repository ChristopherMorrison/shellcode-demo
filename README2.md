# Modern Shellcode
This is a collection of information on relatively modern shellcode.

This will not contain any other binary exploitation topics to remain succinct.

# 1. Pre-requisites
Everything in the first week should be review for incoming students.

Concepts:
- linux
- basic assembly (arm's easier) / how processors work + disassembling
- basic understanding of C
- basic understanding of process memory
- basic understanding of how an ELF is layed out
- basic networks + network programming

Tools:
- gcc + gdb
- ghidra
- binutils
- nc

Lab:
1. Setup a linux system (debian/ubuntu or even a container) that can use the tools
1. `A` Write a program in C that connects to an nc server and writes a message, include debug symbols
1. Disassemble the above program and:
    - note where any data you have not created appeared
    - not which sections various parts of your source have been put in  (strings vs functions, etc)

# 2. What is Shellcode
- syscalls
    - what is a syscall?
    - syscall table(s)
- bytes vs elf
    - show how an elf is layed out in objdump, also show size
    - take the same after extracting .text into a binary file and show size difference
- data

# 3. How to make shellcode
- from raw assembly
- from c code - no stdlib
- from c code - no std start
- from higher level tooling (pwnlib)

# 4. Shellcode Examples 1
- basic write + exit hello world
- tcp bindshell + nc
- tcp reverse shell + nc

# 5. Shellcode under filter constraints
- alphanumeric filters
- unicode filters
- null filters
- arbitrary filters -- emoji, specific languages, a different architecture

# 6. Shellcode analysis + re
- Identifying the architecture
- finding the entrypoint
- ghidra for shellcode analysis
- identifying public libraries staticly linked

# 7. multi-stage shellcodes
- encoding / enterpreting
    - whatever that defcon presentation was with the crazy decoders
- multi-architecture shellcode stages
- shellcode IPC
- (multiple defcon presentations)

# Beyond the basics
- symbolic+IR tooling for shellcode generation
    - angr+vex
    - pcode
- shellcoder's handbook -- details for specific platforms
- bootstraping dlopen -- maybe separate this out for malware analysis
