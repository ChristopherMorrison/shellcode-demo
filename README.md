# Shellcode Demo
A quick demo of how to shellcode for CTF's

## Part 1: C hello-world
1. `make hello-world`
2. run `hello-world` just to see what it does
3. `file hello-world` -> It's an elf, a very standard executable storage format
4. `ldd hello-world` -> it depends on libc and ld at runtime
5. `readelf -Wl hello-world` -> it has a moderately complex memory layout
6. `objdump -d hello-world` -> for how small our `main()` is, this is a lot of extra code

So how do we do the following:
1. make our code closer to what we write in C
2. make our code not depend on other libraries like libc

Enter the following:
- `-static`
- `-nostartfiles`
- syscalls

##  Part 2: hello-syscall
1. `make hello-syscall`
2. Run it, looks identical to `hello-world`, right?
3. `file hello-world hello-syscall`, static and no interpreter
4. `ldd hello-syscall`, confirms static
5. `readelf -Wl hello-syscall`, we have way less now and we only really need .text (asm code) and .rodata (strings)
6. `objdump -d hello-syscall`, look how much less there is, only two functions

So now we have pretty much the min we need to just copy these bytes into memory and run them.

Time to cook some shellcode up.

## Part 3: hello-shellcode
What we're going to do here is just take the .text and .rodata parts of hello-syscall, and rip them into a separate file using objcopy

1. `make hello-shellcode`
    - read this example for the exact happenings
2. `file hello-shellcode`, data means random bytes
3. `make inspect-shellcode`
    - this is a fancy objdump call
    - you can see our two functions and the string from rodata
4. `xxd hello-shellcode`
    - just a more visual layout of what we're playing with
    - can still see that string at the end
5. `make runner`
6. `strace ./runner`
    - we're just looking at the last few lines here, we say 'executing' then die
7. `strace ./runner hello-shellcode`
    - see that our shellcode just ran via strace

## Part 4: Doing it from nasm
This is a quick demo of how to maintain it as raw asm code. This is much harder than C to write and maintain but can be much more optimized.

1. `make hello-nasm`
2. `strace ./runner hello-nasm`
    - notice the exit code in strace

Again, just making this one was a pain, I had to look it up and convert it to nasm to make this work.

## Part 5: Doing it from shellcraft
Shellcraft is a lot easier to make shellcode in, and it's about as maintainable too. However it only does linear execution of non-dependent syscalls. That means we can't do any real flow control or pass file handles around.

1. `make hello-shellcraft`
2. Take a look at the `hello-shellcraft.asm` file that is generated
    - just an ASM source that we'll compile using shellcraft later in the script
3. `strace ./runner hello-shellcraft.bin`
    - looks the same
4. `xxd hello-shellcode.bin`
5. `make inspect-shellcraft`
    - ends up being a bit more optimized than the C version

## Part 6: Closing thoughts and CTF notes
Generally during CTF's shellcraft is going to be fine as we'll just want to do something along the lines of `cat flag.txt`. In the more advanced CTF problems such as those google puts out, shellcode may be used as an initial stage for inter-process pwn problems. In these more advanced cases, C is probably the better method.

# References
- https://www.exploit-db.com/docs/english/21013-shellcoding-in-linux.pdf
- http://jgeralnik.github.io/writeups/2020/09/05/writeonly/