#include <fcntl.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <stddef.h>
#include <stdio.h>

int main(int argc, char** argv) {
    int shellcode_fd = openat(AT_FDCWD, argv[1], O_RDONLY);
    if (shellcode_fd > 0) puts("opened file");

    struct stat stat_result;
    stat(argv[1], &stat_result);
    void* loaded_file = mmap(NULL, stat_result.st_size, PROT_EXEC|PROT_READ|PROT_WRITE, MAP_PRIVATE, shellcode_fd, 0);
    if (loaded_file != NULL) puts("loaded file");

    typedef void(*entrypoint_t)(void);
    entrypoint_t entrypoint = loaded_file;
    puts("executing");
    entrypoint();
}
