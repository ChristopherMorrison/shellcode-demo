[SECTION .text]
bits 64; comment this out for x86 vs x86_64
global _start
_start:
    mov rax, 60; SYS_exit = 60
    mov rdi, 1337; exit code 1337
    syscall
