#!/usr/bin/env python3
from pwn import *

shellcode_src = shellcraft.write(1, 'hello-world')
shellcode_src += shellcraft.exit(0)

with open('hello-shellcraft.asm', 'w') as fp:
    fp.write(shellcode_src)

shellcode = asm(shellcode_src)

with open('hello-shellcraft.bin', 'wb') as fp:
    fp.write(shellcode)
