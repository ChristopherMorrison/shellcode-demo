#
# Meta
#
CC_STATIC_FLAGS := -static -nostartfiles -Wl,--build-id=none

all: hello-world hello-shellcode runner hello-shellcraft hello-nasm
.PHONY: all

clean:
	rm -rf hello-world
	rm -rf hello-syscall
	rm -rf hello-shellcode
	rm -rf hello-nasm.o hello-nasm
	rm -rf hello-shellcraft.bin hello-shellcraft.asm
	rm -rf runner
.PHONY: clean

#
# Runner utility
#
runner: runner.c
	$(CC) runner.c -o runner -static

#
# Normal C Hello World
#
hello-world: hello-world.c
	$(CC) hello-world.c -o hello-world

#
# C Syscall hello world & C based shellcode
#
hello-syscall: hello-syscall.c
	$(CC) $(CC_STATIC_FLAGS) hello-syscall.c -o hello-syscall 

inspect-syscall: hello-syscall
	objdump -d hello-syscall|less

hello-shellcode: hello-syscall
	objcopy -j .text -j .rodata hello-syscall hello-shellcode -O binary

inspect-shellcode: hello-shellcode
	objdump --target=binary -m i386:x86-64 -D hello-shellcode

#
# Hand assembled hello world
# TODO: why is this a com file
#
hello-nasm: hello-nasm.asm
	nasm -f elf hello-nasm.asm
	objcopy -j .text hello-nasm.o hello-nasm -O binary

inspect-nasm: hello-nasm
	objdump --target=binary -m i386:x86-64 -D hello-nasm

#
# Pwntools/Shellcraft asm hello world
#
hello-shellcraft: hello-shellcraft.py
	python3 ./hello-shellcraft.py

inspect-shellcraft:
	objdump --target=binary hello-shellcraft.bin -m i386:x86-64 -D

#
# Shellcode DB example (TODO: replace this with the hand-assembled)
#
inspect-exec:
	objdump --target=binary exec_sh.x64.bin -m i386:x86-64 -D

