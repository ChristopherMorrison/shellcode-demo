#include <unistd.h>
#include <sys/syscall.h>

void _start() {
    // shellcode example
    syscall(SYS_write, 1, "hello world\n", 12);
    syscall(SYS_exit, 0);
}
